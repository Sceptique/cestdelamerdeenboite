# coding: utf-8

module NN

  class Memory
    class UndefinedSegment < Exception ;end

    def initialize
      @o = {}
    end

    TO_S_PREFIX = "Memory"
    TO_S_ROUND = 2
    TO_S_BRACES = "()"
    TO_S_JOIN = ", "
    def to_s(round=TO_S_ROUND, braces=TO_S_BRACES, join=TO_S_JOIN, prefix=TO_S_PREFIX)
      string = @o.map{|k,v| "#{k}=#{v[:v].round(round)}x#{v[:w]}"}.join(join)
      prefix + braces[0] + string + braces[-1]
    end

    def associate o, v
      raise ArgumentError, "v must be a Numeric value" if not v.is_a? Numeric
      if @o[o].nil?
        @o[o] = {:v => v, :w => 1}
      else
        value = ponderated_value_for(o) + v
        ponderation = weight_for(o) + 1.0
        @o[o][:v] = value / ponderation
        increment_weight_for o
      end
      @o[o]
    end

    def weight_for o
      raise UndefinedSegment if @o[o].nil?
      @o[o][:w]
    end

    def value_for o
      raise UndefinedSegment if @o[o].nil?
      @o[o][:v]
    end

    def [](o)
      raise UndefinedSegment if @o[o].nil?
      @o[o]
    end

    def ponderated_value_for o
      value_for(o) * weight_for(o)
    end

    private
    def increment_weight_for o
      raise UndefinedSegment if @o[o].nil?
      @o[o][:w] += 1
    end

  end

end
