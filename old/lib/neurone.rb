# coding: utf-8

require_relative 'memory'

module NN

  class InvalidNeuroneAction < Exception; end
  class InvalidBindType < Exception; end

  class Neurone
    attr_reader :name, :f, :b, :m, :l
    alias :links :l
    alias :forward :f
    alias :backward :b

    # @param: name [] name the neurone. should be uniq.
    # @param: opts [Hash] contain the next arguments. only forward is **required**
    # @param: forward [lambda] returns a Numeric. it is the neurone action.
    # @param: backward [lambda] action called after forward.
    # @param: links [Array or Hash] Array of Neurones
    #
    # About f/b arguments:
    # - forward should have the node + exactly the same number of argument than passed to the inputs argument for {#input} + o
    # - backward should have the same arguments, plus "computed" (result of the :f lambda)
    def initialize name, opts={forward: nil, backward: nil, links: {}}
      @name = name
      @f = opts[:forward]
      @b = opts[:backward]
      links = opts[:links] || {}
      links = Hash[links.map{|l| [l.name, l]}] if links.is_a? Array
      @l = links
      @m = Memory.new
    end

    def bind arg
      if arg.is_a? Hash
        raise InvalidBindType, "Cannot bind with a Hash (not defined)"
      elsif arg.is_a? Neurone
        @l[arg.name] = arg
      else
        raise InvalidBindType, "Cannot bind this kind of object '#{arg.class}'"
      end
    end

    # the number of inputs parameters must be the same than excepted by f
    def input o, *inputs
      puts "node #{@name} << #{o} #{inputs.join(', ')}" if $verbose
      computed = @f.call(self, o, *inputs)
      raise InvalidNeuroneAction, "must returns a numeric" if not computed.is_a? Numeric
      @b.call(self, o, *inputs, computed) if @b
      @m.associate(o, computed)
      self
    end

    def output o
      begin
        @m.ponderated_value_for(o)
      rescue Memory::UndefinedSegment => e
        0
      end
    end

    def power o
      begin
        @m.weight_for o
      rescue Memory::UndefinedSegment => e
        0
      end
    end

    def value o
      begin
        @m.value_for o
      rescue Memory::UndefinedSegment => e
        0
      end
    end

    def [](o)
      begin
        @m[o]
      rescue Memory::UndefinedSegment => e
        {v: 0, m: 0}
      end
    end

    def to_s
      "NN::Neurone<Memory::<#{@m}>>"
      @m.to_s
    end

  end

end
